/**@<tokens.h>::**/

enum {
	ID = 1025,
	DEC,
	OCTAL,
	CMDSEP,
};

enum {
	SYNTAX_ERR = -64,
};
